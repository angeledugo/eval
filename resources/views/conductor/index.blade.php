@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
        <div class="col-md-3 pull-right">
            {!! Html::link(route('conductor.create'), 'Crear', array('class' => 'btn btn-info btn-md pull-right')) !!}
        </div>
		<div class="col-md-10 col-md-offset-1">
        @if (Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif
        @if(!$conductores->isEmpty())
          <table class="table table-bordered">
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
              @foreach ($conductores as $conductor)
                  <tr>
                    <td width="500">{{ $conductor->nombre }}</td>
                    <td width="500">{{ $conductor->apellido }}</td>
                    
                    <td width="60" align="center">
                      {!! Html::link(route('conductor.edit', $conductor->id), 'Edit', array('class' => 'btn btn-success btn-md')) !!}
                    </td>
                    <td width="60" align="center">
                      {!! Form::open(array('route' => array('conductor.destroy', $conductor->id), 'method' => 'DELETE')) !!}
                          <button type="submit" class="btn btn-danger btn-md">Delete</button>
                      {!! Form::close() !!}
                    </td>
                  </tr>
              @endforeach
          </table>
          <?php echo $conductores->render(); ?>
      @endif
		</div>
	</div>
</div>
@endsection
