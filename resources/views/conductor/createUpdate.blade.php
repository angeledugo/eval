@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
		
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Editar Conductor</div>
 
                
 
				<div class="panel-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
					{!! Form::open(['route' => ['conductor.update',$conductor->id],'method' => 'PUT']) !!}
 
							<div class="form-group">
								{!! Form::text('nombre', $conductor->nombre,['class' => 'form-control','placeholder' => 'Nombre del conductor']) !!}
							</div>
                             
							<div class="form-group">
								{!! Form::text('apellido', $conductor->apellido,['class' => 'form-control','placeholder' => 'Apellido del conductor']) !!}
							</div>
                            <div class="form-group">
                            {!! Form::text('documento', $conductor->documento,['class' => 'form-control','placeholder' => 'Numero de documento']) !!}
                            </div>
                            <div class="form-group">
                            {!! Form::date('fecha_nac', $conductor->fecha_nac,['class' => 'form-control','placeholder' => 'Fecha de Nacimiento']) !!}
                            </div>
                            <div class="form-group">
                            {!! Form::select('genero', ['Hombre' => 'Hombre', 'Mujer' => 'Mujer'], $conductor->genero,['class' => 'form-control']); !!}
                            </div>
                            <div class="form-group">
                            {!! Form::select('vehiculo_id', array_flip($vehiculo), $conductor->vehiculos_id,['class' => 'form-control']); !!}
                            </div>
							<div class="form-group">
								<input type="submit" class="btn btn-success">
							</div>
 
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection