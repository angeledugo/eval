@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
      @if (Session::has('message'))
          <div class="alert alert-success">{{ Session::get('message') }}</div>
      @endif
      @if(!$vehiculos->isEmpty())
          <table class="table table-bordered">
              <tr>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
              @foreach ($vehiculos as $vehiculo)
                  <tr>
                    <td width="500">{{ $vehiculo->marca }}</td>
                    <td width="500">{{ $vehiculo->modelo }}</td>
                    <td width="60" align="center">
                      {!! Html::link(route('vehiculo.edit', $vehiculo->id), 'Edit', array('class' => 'btn btn-success btn-md')) !!}
                    </td>
                    <td width="60" align="center">
                      {!! Form::open(array('route' => array('vehiculo.destroy', $vehiculo->id), 'method' => 'DELETE')) !!}
                          <button type="submit" class="btn btn-danger btn-md">Delete</button>
                      {!! Form::close() !!}
                    </td>
                  </tr>
              @endforeach
          </table>
          <?php echo $vehiculos->render(); ?>
      @endif
		</div>
	</div>
</div>
@endsection
