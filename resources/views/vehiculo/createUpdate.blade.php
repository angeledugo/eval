@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
        
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Editar Vehiculo
                    <div class="col-md-3 pull-right">
                        {!! Html::link(route('vehiculo.create'), 'Crear', array('class' => 'btn btn-info btn-md pull-right')) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
 
                
 
				<div class="panel-body">
				@if (Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
					{!! Form::open(['route' => ['vehiculo.update',$vehiculo->id],'method' => 'PUT']) !!}
 
							<div class="form-group">
                                {!! Form::text('matricula', $vehiculo->matricula, ['class' => 'form-control']) !!}
							</div>
 
							<div class="form-group">
                                {!! Form::select('marca', ['Ford' => 'Ford', 'Chevrolet' => 'Chevrolet'], $vehiculo->marca,['class' => 'form-control']); !!}

							</div>

							<div class="form-group">
								{!! Form::text('modelo', $vehiculo->modelo, ['class' => 'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::text('color', $vehiculo->color, ['class' => 'form-control']) !!}
							</div>

						<div class="form-group">

                            {!! Form::select('tipo', ['Sedan' => 'Sedan', 'Chevrolet' => 'Coupet'], $vehiculo->tipo,['class' => 'form-control']); !!}
								
							</div>
 
							<div class="form-group">
								<input type="submit" class="btn btn-success" value="Guardar">
							</div>
 
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection