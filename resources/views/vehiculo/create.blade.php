@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
		
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Crear Vehiculo</div>
 
                
 
				<div class="panel-body">
					@if (Session::has('message'))
						<div class="alert alert-success">{{ Session::get('message') }}</div>
					@endif
					{!! Form::open(['route' => 'vehiculo.store','method' => 'post']) !!}
 
							<div class="form-group">
								<input type="text" class="form-control" name="matricula" placeholder="matricula">
							</div>
 
							<div class="form-group">
								<select id="" name="marca" class="form-control">
									<option value="">Seleccione</option>
									<option value="ford">Ford</option>
									<option value="chevrolet">Chevrolet</option>
								</select>
							</div>

							<div class="form-group">
								<input type="text" name="modelo" class="form-control" placeholder="modelo">
							</div>
							<div class="form-group">
								<input type="text" name="color" class="form-control" placeholder="Color">
							</div>

						<div class="form-group">
								<select id="" name="tipo" class="form-control">
									<option value="Sedan">Sedan</option>
									<option value="Coupet">Coupet</option>
								</select>
							</div>
 
							<div class="form-group">
								<input type="submit" class="btn btn-success">
							</div>
 
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection