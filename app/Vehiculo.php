<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    //
    public $timestamps = false;

    protected $fillable = ['modelo', 'marca','color','tipo'];

    public function conductor(){
       return $this->BelongTo('App\Conductor');
    }
}
