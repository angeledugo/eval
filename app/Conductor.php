<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class conductor extends Model
{
    //
    protected $table = 'conductores';
    public $timestamps = false;
    protected $fillable = ['nombre', 'apellido', 'documento','fecha_nac','genero'];
    protected $hidden = ['vehiculo_id'];

    public function vehiculos(){
       return $this->hasMany('App\Vehiculo','id');
    }
}
