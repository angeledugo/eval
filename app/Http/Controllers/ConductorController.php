<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conductor;
use App\Vehiculo;


class ConductorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
         return view("conductor.index")->with('conductores', \App\Conductor::paginate(2)->setPath('conductor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $vehiculo = \App\Vehiculo::pluck('id', 'modelo')->all();
        return view('conductor.create',compact('vehiculo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $vehiculo = new Conductor;
        
        $vehiculo->nombre = $request->input('nombre');
        $vehiculo->apellido = $request->input('apellido');
        $vehiculo->fecha_nac = $request->input('fecha_nac');
        $vehiculo->documento = $request->input('documento');
        $vehiculo->genero = $request->input('genero');
        $vehiculo->vehiculo_id = $request->input('vehiculo_id');

        $vehiculo->save();

        return redirect('conductor/create')->with('message', 'conductor agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $vehiculo = \App\Vehiculo::pluck('id', 'modelo')->all();
        $conductor = Conductor::findOrFail($id);
        
        return view('conductor.createUpdate')->with(['vehiculo' => $vehiculo,'conductor' => $conductor ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $conductor = \App\Conductor::find($id);

        $conductor->nombre = $request->input('nombre');
        $conductor->apellido = $request->input('apellido');
        $conductor->genero = $request->input('genero');
        $conductor->documento = $request->input('documento');
        $conductor->vehiculo_id = $request->input('vehiculo_id');

        $conductor->save();

        return redirect()->route('conductor.edit', ['post' => $id])->with('message', 'conductor updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $conductor = \App\Conductor::find($id);
 
        $conductor->delete();
    
        return redirect()->route('conductor.index')->with('message', 'conductor deleted');
    }
}
