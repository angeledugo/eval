<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehiculo;

class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("vehiculo.index")->with('vehiculos', \App\Vehiculo::paginate(2)->setPath('vehiculo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        return view('vehiculo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $vehiculo = new Vehiculo;
        
        $vehiculo->matricula = $request->input('matricula');
        $vehiculo->marca = $request->input('marca');
        $vehiculo->modelo = $request->input('modelo');
        $vehiculo->color = $request->input('color');
        $vehiculo->tipo = $request->input('tipo');

        $vehiculo->save();

        return redirect('vehiculo/create')->with('message', 'Vehiculo agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehiculo = Vehiculo::findOrFail($id);
        return view('vehiculo.createUpdate')->withVehiculo($vehiculo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehiculo = \App\Vehiculo::find($id);

        $vehiculo->matricula = $request->input('matricula');
        $vehiculo->modelo = $request->input('modelo');
        $vehiculo->color = $request->input('color');
        $vehiculo->tipo = $request->input('tipo');

        $vehiculo->save();

        return redirect()->route('vehiculo.edit', ['post' => $id])->with('message', 'Vehiculo updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehiculo = \App\Vehiculo::find($id);
 
        $vehiculo->delete();
    
        return redirect()->route('vehiculo.index')->with('message', 'Vehiculo deleted');
    }
}
